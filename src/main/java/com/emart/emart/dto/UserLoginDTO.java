package com.emart.emart.dto;

import java.io.Serializable;

public class UserLoginDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7919463422822424633L;
	
	private String userName;
	
	private String password;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserLoginDTO [userName=" + userName + ", password=" + password + "]";
	}
	
}
