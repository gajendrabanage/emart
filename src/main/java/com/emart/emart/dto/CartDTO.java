/**
 * 
 */
package com.emart.emart.dto;

import java.io.Serializable;

/**
 * @author Vignesh
 *
 */
public class CartDTO implements Serializable {

	/**
	 * Generated Serial version Id
	 */
	private static final long serialVersionUID = 3958419316828835682L;

	/**
	 * Auto generated id field
	 */
	private Long id;

	/**
	 * Foreign key with User table
	 */
	private Long userId;

	/**
	 * Foreign key with products table
	 */
	private Long productId;

	/**
	 * quantity for the selected item
	 */
	private int quantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Cart [id=" + id + ", userId=" + userId + ", productId=" + productId + ", quantity=" + quantity + "]";
	}
}
