package com.emart.emart.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.emart.emart.entity.UserAccount;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

	
	UserAccount findByUserNameAndPassword(String userName, String password);
}
