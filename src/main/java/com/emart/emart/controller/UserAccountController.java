package com.emart.emart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emart.emart.dto.UserAccountDTO;
import com.emart.emart.dto.UserLoginDTO;
import com.emart.emart.service.UserAccountService;
import com.emart.emart.utility.ConstantMessage;

@RestController
@RequestMapping("/user")
public class UserAccountController {

	@Autowired
	private UserAccountService userAccountService;
	
	@PostMapping("/signup")
	public ResponseEntity<?> createUser(@RequestBody UserAccountDTO userAccountDTO) {

		boolean response = userAccountService.createUser(userAccountDTO);
		if (response)
			return ResponseEntity.status(HttpStatus.CREATED).body(ConstantMessage.userCreactedSuccess);
		else
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ConstantMessage.userCreactedSuccess);

	}
	
	
	@PostMapping("/login")
	public ResponseEntity<?> loginUser(@RequestBody UserLoginDTO userLoginDTO) {

		boolean response = userAccountService.loginUser(userLoginDTO);
		if (response)
			return ResponseEntity.status(HttpStatus.OK).body(ConstantMessage.loginSuccessfully);
		else
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ConstantMessage.loginFail);
	}

	@PostMapping("/logintest")
	public boolean loginUsertest(@RequestBody UserLoginDTO userLoginDTO) {

		return userAccountService.loginUser(userLoginDTO);
	}

	
}
