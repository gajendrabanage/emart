package com.emart.emart.service;

import com.emart.emart.dto.UserAccountDTO;
import com.emart.emart.dto.UserLoginDTO;

public interface UserAccountService {

	boolean createUser(UserAccountDTO userAccountDTO);

	boolean loginUser(UserLoginDTO userLoginDTO);

}
