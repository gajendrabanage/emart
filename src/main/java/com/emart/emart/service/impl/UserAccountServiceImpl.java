package com.emart.emart.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emart.emart.dto.UserAccountDTO;
import com.emart.emart.dto.UserLoginDTO;
import com.emart.emart.entity.UserAccount;
import com.emart.emart.repository.UserAccountRepository;
import com.emart.emart.service.UserAccountService;

@Service
public class UserAccountServiceImpl implements UserAccountService{
	
	@Autowired
	private UserAccountRepository userAccountRepository;
	
	@Override
	public boolean createUser(UserAccountDTO userAccountDTO) {
		
		UserAccount userAccount = new UserAccount();
		userAccount.setAddress(userAccountDTO.getAddress());
		userAccount.setContactNo(userAccountDTO.getContactNo());
		userAccount.setEmailId(userAccountDTO.getEmailId());
		userAccount.setFirstName(userAccountDTO.getFirstName());
		userAccount.setLastName(userAccountDTO.getLastName());
		userAccount.setPassword(userAccountDTO.getPassword());
		userAccount.setUserName(userAccountDTO.getUserName());
		UserAccount savedUserAccount = userAccountRepository.save(userAccount);	
		return savedUserAccount!= null ? true : false; 
	}

	@Override
	public boolean loginUser(UserLoginDTO userLoginDTO) {

		UserAccount userAccount = userAccountRepository.findByUserNameAndPassword(userLoginDTO.getUserName(),
				userLoginDTO.getPassword());
		return userAccount!= null ? true : false;
	}

	
}
